class AddDescriptionToPreventativeMaintenances < ActiveRecord::Migration[6.0]
  def change
    add_column :preventative_maintenances, :description, :string
    reversible do |direction|
      direction.up { PreventativeMaintenance.where(description: nil).update_all(description: "temp") }
    end
    change_column :preventative_maintenances, :description, :string, null: false
  end
end
