class AddFieldsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :work_phone, :string
    add_column :users, :cell_phone, :string
    add_column :users, :rish_number, :string
  end
end
