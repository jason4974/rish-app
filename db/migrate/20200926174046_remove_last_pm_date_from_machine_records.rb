class RemoveLastPmDateFromMachineRecords < ActiveRecord::Migration[6.0]
  def change
    remove_column :machine_records, :last_pm_date
  end
end
