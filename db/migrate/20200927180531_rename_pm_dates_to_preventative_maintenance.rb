class RenamePmDatesToPreventativeMaintenance < ActiveRecord::Migration[6.0]
  def change
    rename_table :pm_dates, :preventative_maintenances
  end
end
