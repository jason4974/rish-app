class CreateMachineRecords < ActiveRecord::Migration[6.0]
  def change
    create_table :machine_records do |t|
      t.string :model_number
      t.string :serial_number
      t.date :last_pm_date
      t.decimal :hours

      t.timestamps
    end
  end
end
