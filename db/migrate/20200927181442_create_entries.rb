class CreateEntries < ActiveRecord::Migration[6.0]
  def change
    create_table :entries do |t|
      t.references :machine_record, null: false, foreign_key: true
      t.string :hours

      t.timestamps
    end
  end
end
