class RenameModelNumberToMachineName < ActiveRecord::Migration[6.0]
  def change
    rename_column :machine_records, :model_number, :name
  end
end
