class CreatePmDates < ActiveRecord::Migration[6.0]
  def change
    create_table :pm_dates do |t|
      t.date :date, null: false
      t.references :machine_record, null: false, foreign_key: true

      t.timestamps
    end
  end
end
