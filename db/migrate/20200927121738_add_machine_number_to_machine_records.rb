class AddMachineNumberToMachineRecords < ActiveRecord::Migration[6.0]
  def change
    add_column :machine_records, :machine_number, :string
  end
end
