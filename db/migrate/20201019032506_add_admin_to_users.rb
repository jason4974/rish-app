class AddAdminToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :admin, :boolean
    reversible do |direction|
      direction.up { User.where(admin: nil).update_all(admin: false) }
    end
    change_column :users, :admin, :boolean, null: false
  end
end
