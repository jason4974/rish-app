class RemoveHoursFromMachineRecords < ActiveRecord::Migration[6.0]
  def change
    remove_column :machine_records, :hours
  end
end
