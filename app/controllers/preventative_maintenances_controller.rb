class PreventativeMaintenancesController < ApplicationController

  def create
    @machine_record = MachineRecord.find(params[:machine_record_id])
    @preventative_maintenance = @machine_record.preventative_maintenances.create(preventative_maintenance_params)


    respond_to do |format|
      if @preventative_maintenance.save
        format.html { redirect_to machine_record_path(@machine_record), notice: 'PM submitted' }
      else
        format.html { redirect_to machine_record_path(@machine_record), alert: 'PM could not be saved' }
      end
    end
  end

  def update
    respond_to do |format|
      @machine_record = MachineRecord.find(params[:machine_record_id])
      @preventative_maintenance = @machine_record.preventative_maintenances.find(params[:id])

      if @preventative_maintenance.update(preventative_maintenances_params)
        format.html { redirect_to request.referrer, notice: 'PM was successfully updated' }
        format.json { render :show, status: :ok, location: @preventative_maintenance }
      else
        format.html { render :edit, alert: 'PM Could not be saved' }
        format.json { render json: @preventative_maintenance.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @machine_record = MachineRecord.find(params[:machine_record_id])
    @preventative_maintenance.destroy
    respond_to do |format|
      format.html { redirect_to request.referrer, info: 'PM successfully deleted' }
      format.json { head :no_content }
    end
  end

  private
    def preventative_maintenance_params
      params.require(:preventative_maintenance).permit(:id, :description, :machine_record_id, :date)
    end
end
