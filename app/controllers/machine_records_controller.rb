class MachineRecordsController < ApplicationController
  before_action :set_machine_record, only: [:show, :edit, :update, :destroy]

  # GET /machine_records
  # GET /machine_records.json
  def index
    if current_user.admin?
      @machine_records = MachineRecord.all
    else
      @machine_records = MachineRecord.where(:user_id => current_user.id)
    end
  end

  # GET /machine_records/1
  # GET /machine_records/1.json
  def show
    @machine_record = MachineRecord.find(params[:id])

  end

  # GET /machine_records/new
  def new
    @machine_record = MachineRecord.new
  end

  # GET /machine_records/1/edit
  def edit
  end

  # POST /machine_records
  # POST /machine_records.json
  def create
    @machine_record = MachineRecord.new(machine_record_params)
    @machine_record.user = current_user

    respond_to do |format|
      if @machine_record.save
        format.html { redirect_to @machine_record, notice: 'Machine record was successfully created.' }
        format.json { render :show, status: :created, location: @machine_record }
      else
        format.html { render :new, alert: 'There was a problem creating the machine record.' }
        format.json { render json: @machine_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /machine_records/1
  # PATCH/PUT /machine_records/1.json
  def update
    respond_to do |format|
      if @machine_record.update(machine_record_params)
        format.html { redirect_to @machine_record, notice: 'Machine record was successfully updated.' }
        format.json { render :show, status: :ok, location: @machine_record }
      else
        format.html { render :edit }
        format.json { render json: @machine_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /machine_records/1
  # DELETE /machine_records/1.json
  def destroy
    @machine_record.destroy
    respond_to do |format|
      format.html { redirect_to machine_records_url, notice: 'Machine record was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_machine_record
      @machine_record = MachineRecord.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def machine_record_params
      params.require(:machine_record).permit(:name, :serial_number, :machine_number, :user_id)
    end
end
