class EntriesController < ApplicationController

  def create
    @machine_record = MachineRecord.find(params[:machine_record_id])
    @entry = @machine_record.entries.create(entry_params)
    @entry.save!
    if @entry.save
      redirect_to machine_record_path(@machine_record)
    else
      redirect_to machine_record_path(@machine_record)
    end
  end

  private
    def entry_params
      params.require(:entry).permit(:machine_record_id, :hours)
    end
end
