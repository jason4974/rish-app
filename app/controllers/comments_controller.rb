class CommentsController < ApplicationController

  def create
    @machine_record = MachineRecord.find(params[:machine_record_id])
    @comment = @machine_record.comments.create(comment_params)
    @comment.user = current_user

    respond_to do |format|
      if @comment.save
        format.html { redirect_to machine_record_path(@machine_record), notice: 'Comment submitted' }
      else
        format.html { redirect_to machine_record_path(@machine_record), alert: 'Comment could not be saved'}
      end
    end
  end

  def edit
  end

  def update
    @machine_record = MachineRecord.find(params[:machine_record_id])
    @comment = @machine_record.comments.find(params[:id])

    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @machine_record, notice: 'Comment was successfully updated' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit, alert: 'Comment could not be saved' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to request.referrer, info: 'Comment successfully deleted' }
      format.json { head :no_content }
    end
  end

  private
    def set_comment
      @comment = Comment.find(params[:id])
    end

    def set_machine_record
      @machine_record = MachineRecord.find(params[:machine_record_id])
    end

    def comment_params
      params.require(:comment).permit(:machine_record_id, :body)
    end
end
