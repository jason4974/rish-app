class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  add_flash_types :info

  protected

  def after_signout_redirect
    '/users/sign_in'
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name,
                                                       :last_name,
                                                       :company,
                                                       :rish_number,
                                                       :work_phone,
                                                       :cell_phone])
   devise_parameter_sanitizer.permit(:account_update, keys:  [:first_name,
                                                              :last_name,
                                                              :company,
                                                              :rish_number,
                                                              :work_phone,
                                                              :cell_phone])
  end
end
