class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :rish_number, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :company, presence: true
  validates_uniqueness_of :email

  has_many :machine_records
  has_many :comments

  def admin?
    self.admin
  end

  def name
    name = self.first_name + ' ' + self.last_name
  end
end
