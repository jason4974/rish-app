class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :machine_record

  def date
    self.created_at.strftime('%B %d, %Y')
  end

  def time
    self.created_at.strftime('%I:%M %p')
  end

  def commented_on
    "#{self.date} at #{self.time}"
  end
end
