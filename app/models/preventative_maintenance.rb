class PreventativeMaintenance < ApplicationRecord
  belongs_to :machine_record

  validates_presence_of :date, :description

  def date_short
    self.date.strftime('%D')
  end
end
