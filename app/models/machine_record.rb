class MachineRecord < ApplicationRecord
  has_many  :comments, dependent: :destroy
  has_many  :preventative_maintenances, dependent: :destroy
  has_many  :entries, dependent: :destroy
  belongs_to :user

  # accepts_nested_attributes_for :comments, :preventative_maintenances, :entries

  validates :name, presence: true
  validates :serial_number, presence: true
  # validates_associated :comments, :preventative_maintenances, :entries

  def pms?
    self.preventative_maintenances.any?
  end

  def recent_pms
    if self.pms?
      self.preventative_maintenances.limit(5).order('date desc')
    end
  end

  def last_pm_date
    if self.pms?
      last_pm_date = self.preventative_maintenances.order('date DESC').first.date
      last_pm_date = last_pm_date.strftime('%B %e, %Y')
    else
      "N/A"
    end
  end

  def last_pm_date_short
    if self.pms?
      last_pm_date = self.preventative_maintenances.order('date DESC').first.date
      last_pm_date_short = last_pm_date.strftime('%D')
    else
      "n/a"
    end
  end

  def total_hours
    entries = self.entries.all.to_a
    sum = entries.reduce(0) { |sum, obj| sum + obj.hours.to_f }
    return sum.round(2)
  end

  def entries?
    self.entries.any?
  end

  def recent_entries
    self.entries.limit(5).order('id desc')
  end

end
