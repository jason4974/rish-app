class Entry < ApplicationRecord
  belongs_to :machine_record

  validates :hours, presence: true

  def date
    self.created_at.strftime('%D')
  end

  def time
    self.created_at.strftime('%l:%M %p')
  end

  def time_date_created
    "#{self.date} at #{self.time}"
  end
end
