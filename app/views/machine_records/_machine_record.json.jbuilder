json.extract! machine_record, :id, :model_number, :serial_number, :last_pm_date, :hours, :created_at, :updated_at
json.url machine_record_url(machine_record, format: :json)
