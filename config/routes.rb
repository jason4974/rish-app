Rails.application.routes.draw do
  get 'users/index'
  get 'users/show'
  resources :machine_records do
    resources :comments
    resources :entries
    resources :preventative_maintenances
  end

  resources :comments

  # devise_for :users
  # devise_for :users do
  #   delete '/users/sign_out' => 'devise/sessions#destroy'
  # end

  Rails.application.routes.draw do
  get 'users/index'
  get 'users/show'
      devise_for :users, controllers: {
        sessions: 'users/sessions'
      }
    end

    resources :users, only: [:index, :show]

  root 'machine_records#index'

end
