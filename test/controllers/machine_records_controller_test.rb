require 'test_helper'

class MachineRecordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @machine_record = machine_records(:one)
  end

  test "should get index" do
    get machine_records_url
    assert_response :success
  end

  test "should get new" do
    get new_machine_record_url
    assert_response :success
  end

  test "should create machine_record" do
    assert_difference('MachineRecord.count') do
      post machine_records_url, params: { machine_record: { hours: @machine_record.hours, last_pm_date: @machine_record.last_pm_date, model_number: @machine_record.model_number, serial_number: @machine_record.serial_number } }
    end

    assert_redirected_to machine_record_url(MachineRecord.last)
  end

  test "should show machine_record" do
    get machine_record_url(@machine_record)
    assert_response :success
  end

  test "should get edit" do
    get edit_machine_record_url(@machine_record)
    assert_response :success
  end

  test "should update machine_record" do
    patch machine_record_url(@machine_record), params: { machine_record: { hours: @machine_record.hours, last_pm_date: @machine_record.last_pm_date, model_number: @machine_record.model_number, serial_number: @machine_record.serial_number } }
    assert_redirected_to machine_record_url(@machine_record)
  end

  test "should destroy machine_record" do
    assert_difference('MachineRecord.count', -1) do
      delete machine_record_url(@machine_record)
    end

    assert_redirected_to machine_records_url
  end
end
