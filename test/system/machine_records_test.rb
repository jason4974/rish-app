require "application_system_test_case"

class MachineRecordsTest < ApplicationSystemTestCase
  setup do
    @machine_record = machine_records(:one)
  end

  test "visiting the index" do
    visit machine_records_url
    assert_selector "h1", text: "Machine Records"
  end

  test "creating a Machine record" do
    visit machine_records_url
    click_on "New Machine Record"

    fill_in "Hours", with: @machine_record.hours
    fill_in "Last pm date", with: @machine_record.last_pm_date
    fill_in "Model number", with: @machine_record.model_number
    fill_in "Serial number", with: @machine_record.serial_number
    click_on "Create Machine record"

    assert_text "Machine record was successfully created"
    click_on "Back"
  end

  test "updating a Machine record" do
    visit machine_records_url
    click_on "Edit", match: :first

    fill_in "Hours", with: @machine_record.hours
    fill_in "Last pm date", with: @machine_record.last_pm_date
    fill_in "Model number", with: @machine_record.model_number
    fill_in "Serial number", with: @machine_record.serial_number
    click_on "Update Machine record"

    assert_text "Machine record was successfully updated"
    click_on "Back"
  end

  test "destroying a Machine record" do
    visit machine_records_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Machine record was successfully destroyed"
  end
end
